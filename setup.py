#!/usr/bin/env python

"""Minimal setup.py to help pip with editable installation.

According to the setuptools_ documentation pip might still require
a setup.py file in order to perform editable installs (e.g. `pip install -e`).

To quote:

::

    For the time being, pip still might require a setup.py  file to support
    editable installs.

    A simple script will suffice, for example:

        from setuptools import setup

        setup()

::

To track the status of the effort to support editable installs without the
need for a setup.py file see this issue_ on GitHub.

.. _setuptools: https://setuptools.pypa.io/en/latest/userguide/pyproject_config.html
.. _issue: https://github.com/pypa/setuptools/issues/2816
"""

from setuptools import setup

setup()
